//! Skeleton crate for a texdown to AST project.

#![allow(non_upper_case_globals)]

// Parser library.
#[macro_use]
extern crate nom ;
extern crate clap ;

#[macro_use]
pub mod common ;
pub mod markers ;
pub mod ast ;
pub mod parser ;

pub mod to_texdown ;
