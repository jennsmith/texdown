//! Converts an AST to texDown.

use std::io::Write ;
use std::io::Result as IoRes ;
use std::ffi::OsStr ;
use std::path::Path ;

use clap::{ App, Arg, ArgMatches } ;

use markers::simple::* ;
use markers::rich::* ;

use ast::* ;


/// Specifies the texdown CLAs and how to write to texdown.
pub trait ToTexdown {
  /// Specifies the CLAP sub command for HTML generation.
  fn add_texdown_clas(app: App<'static, 'static>) -> App<'static, 'static> {
    app.about(
      "Options for HTML generation"
    ).author(
      "Adrien Champion <adrien.champion@email.com>"
    ).arg(
      Arg::with_name("blah").long("blah").takes_value(true)
    )
  }

  /// Writes texdown output in a directory.
  ///
  /// `matches` contains the result of the CLAPing for texdown.
  fn to_texdown<'a>(
    & self, src_file: & OsStr, tgt_dir: & Path, matches: & ArgMatches<'a>,
    verb: u64
  ) -> Result<(), String> ;
}

impl ToTexdown for Frames {
  fn to_texdown<'a>(
    & self, src_file: & OsStr, tgt_dir: & Path, _: & ArgMatches<'a>,
    verb: u64
  ) -> Result<(), String> {
    use std::fs::OpenOptions ;
    let mut path_buf = tgt_dir.to_path_buf() ;
    path_buf.push(src_file) ;
    path_buf.set_extension("td") ;
    verb_2!(
      verb,
      "|=| writing output to \"{}\"",
      path_buf.as_path().to_str().unwrap_or("<unknown>")
    ) ;
    verb_3!(
      verb,
      "  | opening \"{}\"...",
      path_buf.as_path().to_str().unwrap_or("<unknown>")
    ) ;
    match OpenOptions::new().create(true).write(true).truncate(true).open(
      & path_buf
    ) {
      Ok(mut file) => {
        verb_3!(verb, "  | done, writing...") ;
        match self.write_texdown(& mut file) {
          Ok(()) => {
            verb_3!(verb, "|=| done.") ;
            Ok(())
          },
          Err(e) => Err(
            format!(
              "while writing file \"{}\"\n\
              (target file for texdown generation)\n\
              > {}", path_buf.as_path().to_str().unwrap_or("<unknown>"), e
            )
          ),
        }
      },
      Err(e) => Err(
        format!(
          "while opening \"{}\"\n\
          (target file for texdown generation)\n\
          > {}", src_file.to_str().unwrap_or("<unknown>"), e
        )
      ),
    }
  }
}

/// Provides a function writing `Self` in a writer in texdown format.
pub trait WriteTexdown {
  /// Writes an `& self` in texDown in a writer.
  fn write_texdown<Writer: Write>(& self, w: & mut Writer) -> IoRes<()> ;
}

/// Provides a function writing `Self` in a writer in texdown format, with
/// identation.
trait ToTexdownIndent {
  /** Writes an `& self` in texDown in a writer.
  
  Parameter `indent` gives the indentation to use for all lines **but the
  first one**.
  */
  fn to_tex_down<Writer: Write>(
    & self, w: & mut Writer, indent: & str
  ) -> IoRes<()> ;
}


impl WriteTexdown for SmplTxt {
  fn write_texdown<Writer: Write>(& self, w: & mut Writer) -> IoRes<()> {
    use ::ast::SmplTxt::* ;

    match * self {
      Seq(ref vec) => {
        for smpl_txt in vec.iter() {
          // Could stack overflow, but the depth of the AST is unlikely to be
          // high enough.
          try!( smpl_txt.write_texdown(w) ) ;
          try!( w.flush() )
        } ;
        Ok(())
      },
      Txt(ref txt) => write!(
        w, "{}", txt
      ),
      Bold(ref txt) => write!(
        w, "{}{}{}", bold_start, txt, bold_end
      ),
      Ital(ref txt) => write!(
        w, "{}{}{}", ita_start, txt, ita_end
      ),
      Code(ref txt) => write!(
        w, "{}{}{}", inline_code_start, txt, inline_code_end
      ),
      Href(ref lhs, ref rhs) => write!(
        w, "{}{}{}{}{}{}",
        href_lhs_start, lhs, href_lhs_end,
        href_rhs_start, rhs, href_rhs_end
      ),
    }

  }
}


impl ToTexdownIndent for RichTxt {
  fn to_tex_down<Writer: Write>(
    & self, w: & mut Writer, indent: & str
  ) -> IoRes<()> {
    use ::ast::RichTxt::* ;

    match * self {
      Smpl(ref smpl_txt) => {
        try!( smpl_txt.write_texdown(w) ) ;
        write!(w, "\n")
      },
      Seq(ref vec) => {
        let mut fst = true ;
        for rich in vec.iter() {
          // Could stack overflow, but the depth of the AST is unlikely to be
          // high enough.
          if fst { fst = false } else {
            try!( write!(w, "{}", indent) )
          } ;
          try!( rich.to_tex_down(w, indent) ) ;
          try!( w.flush() )
        } ;
        Ok(())
      },
      Enum(ref items) => {
        let mut fst = true ;
        for item in items.iter() {
          if fst {
            try!( write!(w, "{} ", enum_item) ) ;
            fst = false
          } else {
            try!( write!(w, "{}{} ", indent, enum_item) ) ;
          } ;
          // Could stack overflow, but the depth of the AST is unlikely to be
          // high enough.
          try!( item.to_tex_down(w, & format!("{}  ", indent)) ) ;
          try!( w.flush() )
        } ;
        Ok(())
      },
      Code(ref language, ref code_lines) => {
        try!( write!(w, "{}", block_code_start) ) ;
        match * language {
          None => (),
          Some(ref language) => try!( write!(w, "[{}]", language) ),
        } ;
        try!( write!(w, "\n") ) ;
        for line in code_lines {
          try!( write!(w, "{}\n", line) )
        } ;
        write!(w, "{}{}\n", indent, block_code_end)
      },
      Table( ref cols ) => if cols.len() > 0 {
        // Print titles.
        try!( write!(w, "{}", table_delim) ) ;
        for col in cols {
          try!( write!(w, " ") ) ;
          try!( col.title.write_texdown(w) ) ;
          try!( write!(w, " |") )
        }
        try!( write!(w, "\n{}", indent) ) ;

        // Print alignment.
        try!( write!(w, "{}", table_delim) ) ;
        for col in cols {
          try!( write!(w, "{}", col.alignment.as_str()) ) ;
          try!( write!(w, "|") )
        }
        try!( write!(w, "\n{}", indent) ) ;

        // Print rows.
        let row_count = cols[0].len() ;
        for row in 0..row_count {
          try!( write!(w, "{}", table_delim) ) ;
          for col in cols {
            try!( write!(w, " ") ) ;
            if col.rows.len() > row {
              try!( col.rows[row].write_texdown(w) )
            }
            try!( write!(w, " |") )
          }
          try!( write!(w, "\n{}", indent) ) ;
        }

        w.flush()
      } else { Ok(()) },
      Quote(ref rich_txt) => {
        let mut string: Vec<u8> = Vec::with_capacity(103) ;
        try!( rich_txt.to_tex_down(& mut string, & indent) ) ;
        try!( write!(w, "\n") ) ;
        for line in String::from_utf8(string).unwrap().lines() {
          try!( write!(w, "{}> {}\n", indent, line) ) ;
          try!( w.flush() )
        } ;
        Ok(())
      },
      NewLine => write!(w, "\n"),
    }

  }
}


impl WriteTexdown for Frame {
  fn write_texdown<Writer: Write>(& self, w: & mut Writer) -> IoRes<()> {
    try!( self.title().write_texdown(w) ) ;
    try!( write!(w, "\n\n  ") ) ;
    try!( self.body().to_tex_down(w, "  ") ) ;
    write!(w, "\n")
  }
}

impl WriteTexdown for Frames {
  fn write_texdown<Writer: Write>(& self, w: & mut Writer) -> IoRes<()> {
    for frame in self.get() {
      try!( frame.write_texdown(w) ) ;
      try!(write!(w, "\n"))
    } ;
    Ok(())
  }
}
