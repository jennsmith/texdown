/*! TexDown AST

There's two distinct data structures to represent text.

- [`SmplTxt`](enum.SmplTxt.html) contains inline formatting that's close to
  markdown.
- [`RichTxt`](enum.RichTxt.html) contains multi-line formatting such as
  enumerations, code blocks, *etc.

A [`Frame`](struct.Frame.html) is a title (simple text) and a body (rich text).

Structure [`Frames`](struct.Frames.html) stores a vector of frames.
*/

use std::io::Write ;
use std::io::Result as IoRes ;

/// A list of frames.
pub struct Frames {
  /// A list of frames.
  frames: Vec<Frame>
}
impl Frames {
  /// Constructor.
  pub fn mk(frames: Vec<Frame>) -> Self {
    Frames { frames: frames }
  }
  /// The frames stored inside.
  #[inline(always)]
  pub fn get(& self) -> & [ Frame ] {
    & self.frames
  }
  /// Creates frames from a file.
  pub fn of_file(source: & str, verbose: bool) -> Result<Self, String> {
    ::parser::frames_of_file(source, verbose)
  }
  /// Writes some frames to a file.
  pub fn write(& self, file: & str) -> IoRes<()> {
    use std::fs::OpenOptions ;
    use to_texdown::WriteTexdown ;
    let file = & mut try!(
      OpenOptions::new().write(true).truncate(true).create(true).open(file)
    ) ;
    self.write_texdown(file)
  }
}

/// Represents a frame.
pub struct Frame {
  /// Title of the frame (simple text).
  title: SmplTxt,
  /// Body of the frame.
  body: RichTxt,
}
impl Frame {
  /// Creates a new frame.
  #[inline(always)]
  pub fn mk(title: SmplTxt, body: RichTxt) -> Self {
    Frame { title: title, body: body }
  }
  
  /// Title of the frame.
  #[inline(always)]
  pub fn title(& self) -> & SmplTxt {
    & self.title
  }
  
  /// Body of the frame.
  #[inline(always)]
  pub fn body(& self) -> & RichTxt {
    & self.body
  }

  /// Writes a frame to a writer.
  pub fn write<Writer: Write>(& self, w: & mut Writer) -> IoRes<()> {
    use to_texdown::WriteTexdown ;
    self.write_texdown(w)
  }

  /// Writes a frame to a file.
  pub fn to_file(& self, file: & str) -> IoRes<()> {
    use std::fs::OpenOptions ;
    let mut file = try!(
      OpenOptions::new().write(true).truncate(true).create(true).open(file)
    ) ;
    self.write(& mut file)
  }
}

/// Alignment enumeration that is used for formatting
#[derive(Debug)]
pub enum Align {
  /// Left Justified
  Left,
  /// Center Justified
  Center,
  /// Right Justified
  Right,
}
impl Align {
  /// The default alignment, when none is given.
  pub fn default() -> Self {
    Align::Left
  }
  pub fn as_str(& self) -> & 'static str {
    use self::Align::* ;
    match * self {
      Left => ":---",
      Center => ":---:",
      Right => "---:",
    }
  }
}

/// Represents a table.
#[derive(Debug)]
pub struct TableCol {
  /// Title of the Table (simple text)
  pub title: SmplTxt,
  /// Vector for the alignment of each column
  pub alignment: Align, 
  /// Vector of row vectors(simple text)
  pub rows: Vec<SmplTxt>,
}
impl TableCol{
  /// Create a new table column
  #[inline(always)]
  pub fn mk(title: SmplTxt, alignment: Align, rows: Vec<SmplTxt>) -> Self {
    TableCol {title:title, alignment: alignment, rows: rows}
  }
  /// Title of the Table Column
  #[inline(always)]
  pub fn title(& self) -> & SmplTxt {
    & self.title
  }
  /// Alignment of Table Column
  #[inline(always)]
  pub fn alignment(& self) -> & Align {
    & self.alignment
  }

  /// Number of rows in the column.
  #[inline(always)]
  pub fn len(& self) -> usize { self.rows.len() }
}
/// Simple text that can only have basic formatting.
#[derive(Debug)]
pub enum SmplTxt {
  /// A sequence of simple text elements.
  Seq( Vec<SmplTxt> ),
  /// Text without any formatting.
  Txt( String ),
  /// Bold text.
  Bold( String ),
  /// Italic text.
  Ital( String ),
  /// Code text (monospace).
  Code( String ),
  /// Hyperlink: first the address, then the text of the link.
  Href( String, String ),
}
impl SmplTxt {
  /// Merges two simple text variants. Used by parsing.
  pub fn add(self, smpl: Self) -> Self {
    use ast::SmplTxt::* ;
    use std::iter::Extend ;
    match self {
      Seq( mut vec ) => match smpl {
        Seq( right ) => {
          vec.extend(right) ;
          Seq(vec)
        },
        right => {
          vec.push(right) ;
          Seq(vec)
        }
      },
      _ => match smpl {
        Seq(vec) => {
          let mut left = vec![ self ] ;
          left.extend(vec) ;
          Seq(left)
        },
        _ => Seq( vec![self, smpl] ),
      },
    }
  }
}

/// Rich text: enumerations, formatted text, blocks, *etc.*
#[derive(Debug)]
pub enum RichTxt {
  /// Can be just simple text.
  Smpl( SmplTxt ),
  /// Can be a sequence of rich text.
  Seq( Vec<RichTxt> ),
  /// Can be an enumeration.
  Enum( Vec<RichTxt> ),
  /// Can be a block of code. First is the language, if any, second are the
  /// lines of code.
  Code( Option<String>, Vec<String> ),
  /// Can be a quoted block.
  Quote( Box<RichTxt> ),
  /// Newline.
  NewLine,
  /// Table.
  Table( Vec<TableCol>),
}
impl RichTxt {
  /// Merges two rich text variants. Used by parsing.
  pub fn add(self, rich: Self) -> Self {
    use ast::RichTxt::* ;
    match self {
      Table(_) | Smpl(_) | NewLine => match rich {
        Seq(vec) => {
          let mut left = vec![ self ] ;
          left.extend(vec) ;
          Seq(left)
        },
        _ => Seq( vec![ self, rich ] )
      },
      Seq(mut vec) => match rich {
        Seq(right) => {
          vec.extend(right) ;
          Seq(vec)
        },
        rich => {
          vec.push(rich) ; Seq(vec)
        }
      },
      Enum(mut vec) => {
        match vec.pop() {
          Some(last) => vec.push(last.add(rich)),
          None => vec.push(rich),
        } ;
        Enum(vec)
      },
      Quote(inner) => Quote(Box::new(inner.add(rich))),
      Code(_,_) => match rich {
        Seq(vec) => {
          let mut left = vec![ self ] ;
          left.extend(vec) ;
          Seq(left)
        },
        _ => Seq( vec![self, rich] ),
      },
    }
  }
}





/// Convenience trait to generate simple text.
pub trait ToSmplTxt {
  /// Turns something into plain text.
  fn txt(& self) -> SmplTxt ;
  /// Turns something into bold text.
  fn bold(& self) -> SmplTxt ;
  /// Turns something into italic text.
  fn ital(& self) -> SmplTxt ;
  /// Turns something into inline code (monospace) text.
  fn code(& self) -> SmplTxt ;
  /// Turns something into a link text.
  fn href(& self, Self) -> SmplTxt ;
}

impl<'a> ToSmplTxt for & 'a str {
  fn txt(& self) -> SmplTxt {
    SmplTxt::Txt( self.to_string() )
  }
  fn bold(& self) -> SmplTxt {
    SmplTxt::Bold( self.to_string() )
  }
  fn ital(& self) -> SmplTxt {
    SmplTxt::Ital( self.to_string() )
  }
  fn code(& self) -> SmplTxt {
    SmplTxt::Code( self.to_string() )
  }
  fn href(& self, rhs: Self) -> SmplTxt {
    SmplTxt::Href( self.to_string(), rhs.to_string() )
  }
}


/// Convenience trait to generate rich text from simple text.
pub trait ToRichTxt {
  fn as_rich(self) -> RichTxt ;
}
impl ToRichTxt for SmplTxt {
  fn as_rich(self) -> RichTxt {
    RichTxt::Smpl( self )
  }
}
