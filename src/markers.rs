//! Static constants for texDown delimiters.
#![allow(non_upper_case_globals)]

/// Markers for simple text.
pub mod simple {

  /// Reserved characters.
  /// Used by parsing and printing.
  ///
  /// REMEMBER TO UPDATE THIS.
  pub static reserved: & 'static str = "*`|" ;
  /// Special characters, including reserved ones.
  /// Used by parsing and printing.
  ///
  /// REMEMBER TO UPDATE THIS.
  pub static reserved_start: & 'static str = "*`[(|" ;

  /// Start of bold text.
  pub static bold_start : & 'static str = "**" ;
  /// End of bold text.
  pub static bold_end   : & 'static str = "**" ;

  /// Start of italic text.
  pub static ita_start : & 'static str = "*" ;
  /// End of italic text.
  pub static ita_end   : & 'static str = "*" ;

  /// Start of inline code (monospace).
  pub static inline_code_start : & 'static str = "`" ;
  /// End of inline code (monospace).
  pub static inline_code_end   : & 'static str = "`" ;

  /// Hyperlink lhs start.
  pub static href_lhs_start: & 'static str = "[" ;
  /// Hyperlink lhs end.
  pub static href_lhs_end:   & 'static str = "]" ;

  /// Hyperlink rhs start.
  pub static href_rhs_start: & 'static str = "(" ;
  /// Hyperlink rhs end.
  pub static href_rhs_end:   & 'static str = ")" ;

}

/// Markers for rich text.
pub mod rich {

  /// Item start.
  pub static enum_item: & 'static str = "-" ;

  /// Start of block code.
  pub static block_code_start : & 'static str = "```" ;
  /// End of block code.
  pub static block_code_end   : & 'static str = "```" ;

  /// Start of quoted line.
  pub static quote_line: & 'static str = ">" ;

  /// Table cell delimiter.
  pub static table_delim: & 'static str = "|" ;
  /// Table alignment character.
  pub static table_align_char: & 'static str = ":" ;
  /// Table alignment filler.
  pub static table_align_fill: & 'static str = "-" ;

}