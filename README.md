Parser for an extension of markdown for presentation.

The goal is to compile the texdown AST generated to LaTeX and HTML.

For now, all the software does is print back the input file in texdown.

# Markdown features

Supported:

- `*` -- italic
- `**` -- bold
- ` -- code (monospace)
- `[text](link)` -- links
- `-` -- (nested) enumerations

Unsupported:

- allowing markdown in links (parser, ast)
- `>` -- quoted blocks (parser)
- `|` -- tables (parser, ast)
- `[text][key]` -- citation links (parser, ast)
- code blocks (almost supported)
- `#` -- sections (parser, ast)

# Texdown syntax

A frame has the following form:

```md
Frame title **that can use** *markdown*
and can spawn over several lines as long as it's not `indented`

  Body of the frame, indented
  - enumerations **work**
    - nested enumerations recognized by *indentation*
    - see previous item
  - [normal links](melt-banana.net) are supported
```